## [6.7.2] - 2021-05-07
### Modified
- Deployment platform moved from Bintray to JFrog.

## [6.7.1] - 2020-07-24
### Fixed
- Timeout back as non fatal error.

## [6.7.0] - 2020-03-12
### Refactored
- Adapter to work with the new Youboralib version.

## [6.6.0] - 2019-12-09
### Added
- Improvements and some refactor.

## [6.5.0] - 2019-06-21
### Added
- Ads rework

## [6.3.3] - 2019-02-20
### Fixed
- Now fireStop will be sent also when the slot ends

## [6.3.2] - 2019-02-12
### Fixed
- Ad title now shows creative id

## [6.3.1] - 2019-02-01
### Added
- API 16 support

## [6.3.0] - 2019-01-04
### Added
- Youboralib updated to version 6.3.3

## [6.2.2] - 2018-11-22
### Fixed
- adStart sent just for the first ad in a slot

## [6.2.1] - 2018-11-15
### Fixed
- Timeout error not finishing the view
- stop's playhead is now reported correctly
- First ad's adDuration

## [6.2.0] - 2018-11-08
### Added
- Infinity support

## [6.0.2] - 2018-07-31
### Fix
- adError now is showing correctly

## [6.0.1] - 2018-07-23
### Fix
- Playhead is now reported well
- Duration is now reported well
- Now the url is sent when the ad is clicked

## [6.0.0] - 2018-02-20
- First Release