# Freewheel Ad Adapter

## Quick start
Displayed below you will find the steps you must follow to integrate the Youbora ads adapter.

### Using gradle
#### Install the youbora library and the related adapter

Add this to your build.gradle repositories:
```groovy
repositories {
    ...
    maven {
        url  "http://dl.bintray.com/npaw/youbora"
    }
    ...
}
```
Add this to your repositories on build.gradle dependencies:
```groovy
repositories {
    dependencies {
        ...
        implementation 'com.npaw.youbora.adapters:freewheel-adapter:6.5.+'
        ...
    }
}
```

### Adapter initialization
We recommend to set the adAdapter as soon as you have FreeWheel context.

```java
youboraPlugin.setAdsAdapter(new FreewheelAdapter(fwContext));
```

## Supported environments & limitations
This section lists which scenarios have been certified by NPAW’s team, including any existing 
limitations.

#### Supported environments
If the following scenarios do not match with your existing implementation, please get in touch with 
the NPAW team. Additionally, when reaching us, it will be very helpful if you could provide a 
testing example with the plugin integrated, so we can evaluate its behaviour.

#### Player / SDK version

| Freewheel SDK |
|---------------|
| 6.23          |

#### OS Version

| Supported Android Versions |
| :------------------------: |
| 4.1 (Jelly Bean, API level 16) – 9 (Pie, API level 28) |