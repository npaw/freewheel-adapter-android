package com.npaw.youbora.lib6.freewheel

import tv.freewheel.ad.interfaces.*
import tv.freewheel.ad.AdInstance

import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.adapter.AdAdapter

open class FreewheelAdapter(player: IAdContext): AdAdapter<IAdContext>(player), IEventListener {

    private var fwConstants = player.constants
    private var iAdInstance: IAdInstance? = null
    private var currentSlot: ISlot? = null

    private var events = Array(42) {""}

    private var lastPlayhead: Double? = null

    init { registerListeners() }

    override fun registerListeners() {
        super.registerListeners()
        initializeEventArray()
        events.forEach { event -> player?.addEventListener(event, this) }
    }

    private fun initializeEventArray() {
        events[0] = fwConstants.EVENT_REQUEST_INITIATED()
        events[1] = fwConstants.EVENT_REQUEST_COMPLETE()
        events[2] = fwConstants.EVENT_REQUEST_CONTENT_VIDEO_PAUSE()
        events[3] = fwConstants.EVENT_REQUEST_CONTENT_VIDEO_RESUME()
        events[4] = fwConstants.EVENT_SLOT_STARTED()
        events[5] = fwConstants.EVENT_SLOT_ENDED()
        events[6] = fwConstants.EVENT_SLOT_PRELOADED()
        events[7] = fwConstants.EVENT_USER_ACTION_NOTIFIED()
        events[8] = fwConstants.EVENT_ACTIVITY_STATE_CHANGED()
        events[9] = fwConstants.EVENT_EXTENSION_LOADED()
        events[10] = fwConstants.EVENT_SLOT_IMPRESSION()
        events[11] = fwConstants.EVENT_AD_IMPRESSION()
        events[12] = fwConstants.EVENT_AD_IMPRESSION_END()
        events[13] = fwConstants.EVENT_AD_QUARTILE()
        events[14] = fwConstants.EVENT_AD_FIRST_QUARTILE()
        events[15] = fwConstants.EVENT_AD_MIDPOINT()
        events[16] = fwConstants.EVENT_AD_THIRD_QUARTILE()
        events[17] = fwConstants.EVENT_AD_COMPLETE()
        events[18] = fwConstants.EVENT_AD_CLICK()
        events[19] = fwConstants.EVENT_AD_MUTE()
        events[20] = fwConstants.EVENT_AD_UNMUTE()
        events[21] = fwConstants.EVENT_AD_COLLAPSE()
        events[22] = fwConstants.EVENT_AD_EXPAND()
        events[23] = fwConstants.EVENT_AD_PAUSE()
        events[24] = fwConstants.EVENT_AD_RESUME()
        events[25] = fwConstants.EVENT_AD_REWIND()
        events[26] = fwConstants.EVENT_AD_ACCEPT_INVITATION()
        events[27] = fwConstants.EVENT_AD_CLOSE()
        events[28] = fwConstants.EVENT_AD_MINIMIZE()
        events[29] = fwConstants.EVENT_AD_LOADED()
        events[30] = fwConstants.EVENT_AD_STARTED()
        events[31] = fwConstants.EVENT_AD_STOPPED()
        events[32] = fwConstants.EVENT_AD_BUFFERING_START()
        events[33] = fwConstants.EVENT_AD_BUFFERING_END()
        events[34] = fwConstants.EVENT_AD_MEASUREMENT()
        events[35] = fwConstants.EVENT_AD_VOLUME_CHANGED()
        events[36] = fwConstants.EVENT_ERROR()
        events[37] = fwConstants.EVENT_RESELLER_NO_AD()
        events[38] = fwConstants.EVENT_TYPE_CLICK_TRACKING()
        events[39] = fwConstants.EVENT_TYPE_IMPRESSION()
        events[40] = fwConstants.EVENT_TYPE_CLICK()
        events[41] = fwConstants.EVENT_TYPE_STANDARD()
    }

    override fun unregisterListeners() {
        events.forEach { event -> player?.removeEventListener(event, this) }
        super.unregisterListeners()
    }

    override fun run(iEvent: IEvent?) {
        iEvent?.let { ievent ->
            val event = ievent.type
            val eventData = ievent.data

            YouboraLog.debug("Freewheel event: $event")

            when (event) {
                fwConstants.EVENT_SLOT_STARTED() -> {
                    currentSlot = player?.getSlotByCustomId(
                            eventData[fwConstants.INFO_KEY_SLOT_CUSTOM_ID()].toString())
                    fireStart()
                }

                fwConstants.EVENT_SLOT_ENDED() -> fireAdBreakStop()
                fwConstants.EVENT_AD_IMPRESSION() -> {
                    iAdInstance = eventData[fwConstants.INFO_KEY_ADINSTANCE()] as IAdInstance
                    fireStart()
                    fireJoin()
                }

                fwConstants.EVENT_AD_PAUSE() -> {
                    lastPlayhead = getPlayhead()
                    firePause()
                }

                fwConstants.EVENT_AD_RESUME() -> {
                    fireResume(HashMap<String, String>().apply {
                        put("adPlayhead", lastPlayhead.toString())
                    })
                }

                fwConstants.EVENT_AD_CLICK() -> fireClick(getUrl())
                fwConstants.EVENT_AD_IMPRESSION_END() -> fireStop()
                fwConstants.EVENT_AD_BUFFERING_START() -> fireBufferBegin()
                fwConstants.EVENT_AD_BUFFERING_END() -> fireBufferEnd()
                fwConstants.EVENT_ERROR() -> {
                    if (flags.isStarted) {
                        errorHandling(eventData[fwConstants.INFO_KEY_ERROR_CODE()].toString(),
                                eventData)
                    }
                }

                fwConstants.EVENT_AD_FIRST_QUARTILE() -> fireQuartile(1)
                fwConstants.EVENT_AD_MIDPOINT() -> fireQuartile(2)
                fwConstants.EVENT_AD_THIRD_QUARTILE() -> fireQuartile(3)
                fwConstants.ERROR_TIMEOUT() -> {
                    fireManifest(ManifestError.NO_RESPONSE, "Timeout error")
                }

                fwConstants.ERROR_NO_AD_AVAILABLE() -> {
                    fireManifest(ManifestError.EMPTY_RESPONSE, "No ad available")
                }

                fwConstants.ERROR_PARSE() -> {
                    fireManifest(ManifestError.WRONG_RESPONSE, "Parse error")
                }
            }
        }
    }

    private fun getUrl(): String? {
        return iAdInstance?.activeCreativeRendition?.primaryCreativRenditionAsset?.url
    }

    private fun errorHandling(errorCode: String, eventData: HashMap<String, Any>) {
        /*if (errorCode == "_e_timeout") {
            fireFatalError(eventData[fwConstants.INFO_KEY_ERROR_INFO()].toString(), errorCode, null)
        } else {*/
            fireError(eventData[fwConstants.INFO_KEY_ERROR_INFO()].toString(), errorCode, null)
        //}
    }

    override fun getPosition(): AdPosition {
       return currentSlot?.let {
           when (it.slotTimePositionClass) {
               IConstants.TimePositionClass.PREROLL -> AdPosition.PRE
               IConstants.TimePositionClass.POSTROLL -> AdPosition.POST
               IConstants.TimePositionClass.MIDROLL -> AdPosition.MID
               else -> AdPosition.UNKNOWN
           }
       } ?: AdPosition.UNKNOWN
    }

    override fun getPlayhead(): Double? {
        return iAdInstance?.playheadTime ?: currentSlot?.playheadTime
    }

    override fun getDuration(): Double? {
        return iAdInstance?.duration ?: currentSlot?.totalDuration
    }

    override fun getAdCreativeId(): String? {
        return if (iAdInstance != null && iAdInstance is AdInstance)
            (iAdInstance as AdInstance).creative.creativeId.toString()
        else
            super.getAdCreativeId()
    }

    override fun getBreaksTime(): MutableList<*>? {
        val slots = player?.temporalSlots
        val breaksTime = ArrayList<Int>()

        slots?.forEach { slot -> breaksTime.add(slot.playheadTime.toInt()) }

        return breaksTime
    }

    override fun getPlayerName(): String? { return "FreeWheel" }
    override fun getAdProvider(): String? { return getPlayerName() }
    override fun getTitle(): String? { return iAdInstance?.adId.toString() }
    override fun getGivenAds(): Int? { return currentSlot?.adInstances?.size }
    override fun getGivenBreaks(): Int? { return player?.temporalSlots?.size }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }

    override fun fireStop(params: MutableMap<String, String>) {
        params["adPlayhead"] = lastPlayhead.toString()
        super.fireStop(params)
    }
}